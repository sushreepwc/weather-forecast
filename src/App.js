import "./App.css";
import WeatherWidget from "./Pages/WeatherWidget";

function App() {
  return (
    <div>
      <WeatherWidget />
    </div>
  );
}

export default App;

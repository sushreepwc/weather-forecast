import React, { useState } from "react";
import axios from "axios";
import { Alert } from "@mui/material";
import { TextField, Button } from "@mui/material";

const LocationInput = ({ setLocation }) => {
  const [inputValue, setInputValue] = useState("");
  const [error, setError] = useState(null);
  const handleLocationSubmit = async (e) => {
    e.preventDefault();
    setError(null);
    try {
      const apiKey = "7b653b3b337f26923f34300de64a14d8";
      const geocodingApiUrl = `http://api.openweathermap.org/geo/1.0/direct?q=${inputValue}&limit=5&appid=${apiKey}`;

      const response = await axios.get(geocodingApiUrl);
      if (response.data.length > 0) {
        const location = response.data[0];
        setLocation({
          latitude: location.lat,
          longitude: location.lon,
        });
      } else {
        setError("No location found");
        // Handle the case where the location is not found
      }
    } catch (error) {
      setError("Error fetching location");
      // Handle errors, e.g., display an error message to the user
    }
  };

  return (
    <div style={{ textAlign: "center", margin: "20px" }}>
      <form onSubmit={handleLocationSubmit}>
        {error && (
          <Alert severity="error" style={{ marginBottom: "20px" }}>
            {error}
          </Alert>
        )}
        <TextField
          label="Enter Location"
          variant="outlined"
          fullWidth
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
          sx={{
            marginBottom: 2,
            backgroundColor: "#ffff",
            borderColor: "black",
          }}
        />
        <br />
        <Button variant="contained" color="primary" type="submit">
          Get Weather
        </Button>
      </form>
    </div>
  );
};

export default LocationInput;

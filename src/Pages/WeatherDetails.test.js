import React from "react";
import { render, fireEvent } from "@testing-library/react";
import WeatherDetails from "./WeatherDetails";

test("renders WeatherDetails component correctly", () => {
  const mockWeatherData = {
    city: { name: "TestCity" },
    list: [
      {
        main: { temp: 25, humidity: 60 },
        wind: { speed: 10 },
      },
    ],
  };

  const { getByText } = render(
    <WeatherDetails
      weatherData={mockWeatherData}
      unit="metric"
      setUnit={() => {}}
    />
  );

  expect(getByText("Weather Details")).toBeInTheDocument();
  expect(getByText("City: TestCity")).toBeInTheDocument();
  expect(getByText("Temperature: 25°C")).toBeInTheDocument();
  expect(getByText("Humidity: 60 g.m-3")).toBeInTheDocument();
  expect(getByText("Speed: 10 Kmh")).toBeInTheDocument();
  expect(getByText("Switch to Fahrenheit")).toBeInTheDocument();
});

test("renders 'No weather data available' when weatherData is not provided", () => {
  const { getByText } = render(
    <WeatherDetails weatherData={null} unit="metric" setUnit={() => {}} />
  );

  expect(getByText("No weather data available")).toBeInTheDocument();
});

test("switches temperature unit on button click", () => {
  const mockWeatherData = {
    city: { name: "TestCity" },
    list: [
      {
        main: { temp: 25, humidity: 60 },
        wind: { speed: 10 },
      },
    ],
  };

  let unit = "metric";
  const setUnitMock = jest.fn((newUnit) => {
    unit = newUnit;
  });

  const { getByText } = render(
    <WeatherDetails
      weatherData={mockWeatherData}
      unit={unit}
      setUnit={setUnitMock}
    />
  );

  fireEvent.click(getByText("Switch to Fahrenheit"));

  expect(setUnitMock).toHaveBeenCalledWith("imperial");
});

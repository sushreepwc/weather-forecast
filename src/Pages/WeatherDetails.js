import React from "react";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";

const WeatherDetails = ({ weatherData, unit, setUnit }) => {
  return (
    <div style={{ textAlign: "center", margin: "20px" }}>
      <Typography variant="h5" gutterBottom>
        Weather Details
      </Typography>
      {weatherData ? (
        <>
          <Typography variant="body1" paragraph>
            City: {weatherData.city.name || "N/A"}
          </Typography>
          <Typography variant="body1" paragraph>
            Temperature: {weatherData.list[0].main?.temp}°
            {unit === "metric" ? "C" : "F"}
          </Typography>
          <Typography variant="body1" paragraph>
            Humidity: {weatherData.list[0].main?.humidity} g.m-3
          </Typography>
          <Typography variant="body1" paragraph>
            Speed: {weatherData.list[0].wind?.speed}{" "}
            {unit === "metric" ? "Kmh" : "Mph"}
          </Typography>
        </>
      ) : (
        <Typography variant="body1" paragraph>
          No weather data available
        </Typography>
      )}
      <Button
        variant="contained"
        color="primary"
        onClick={() => setUnit(unit === "metric" ? "imperial" : "metric")}
      >
        Switch to {unit === "metric" ? "Fahrenheit" : "Celsius"}
      </Button>
    </div>
  );
};

export default WeatherDetails;

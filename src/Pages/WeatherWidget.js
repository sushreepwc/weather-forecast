import React, { useState, useEffect } from "react";
import LocationInput from "./LocationInput";
import WeatherDetails from "./WeatherDetails";
import WeatherCard from "../Components/Card";
import BackgroundImg from "../Assets/wallpaper.jpg";
import { Box, Container, useTheme } from "@mui/material";
import { CircularProgress } from "@mui/material";

// ...

const WeatherWidget = () => {
  const theme = useTheme();
  const [loading, setLoading] = useState(false);
  const [location, setLocation] = useState(""); // Current location state
  const [weatherData, setWeatherData] = useState(null); // Weather data state
  const [forecastData, setForecastData] = useState(null); // Forecast data state
  const [forecastDataResponse, setForecastDataResponse] = useState(null); // Forecast data state
  const [unit, setUnit] = useState("metric"); // Default to metric units
  const fetchWeatherData = async () => {
    setLoading(true);
    try {
      const apiKey = "7b653b3b337f26923f34300de64a14d8";
      const forecastApiUrlResult = `https://api.openweathermap.org/data/2.5/forecast?id=524901&units=${unit}&lat=${location.latitude}&lon=${location.longitude}&appid=${apiKey}`;

      // Make the API request using fetch
      fetch(forecastApiUrlResult)
        .then((response) => {
          if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
          }
          return response.json();
        })
        .then((data) => {
          // Handle the data from the API response
          setWeatherData(data);
          setForecastDataResponse(data.list);
          let filter_forecast_data = [];
          const formated_data = {};
          data.list &&
            data.list.map((data) => {
              const date = new Date(data?.dt * 1000).toLocaleDateString(
                "en-US"
              );
              const formated_data = {
                date: new Date(data?.dt * 1000).toLocaleDateString("en-US"),
                temp: Math.round(data?.main?.temp),
                img_name: data?.weather[0]?.icon,
              };
              if (filter_forecast_data.length === 0) {
                filter_forecast_data.push(formated_data);
              } else {
                let state = false;
                filter_forecast_data.forEach((item) => {
                  return (state = date !== item?.date ? true : false);
                });
                if (state) {
                  filter_forecast_data.push(formated_data);
                }
              }

              formated_data.forecast_data = filter_forecast_data;
            });
          formated_data.forecast_data = filter_forecast_data;
          setForecastData(formated_data.forecast_data);
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
        });
      setLoading(false);
    } catch (error) {
      console.error("Error fetching weather data:", error);
      setLoading(false);
      // Handle errors, e.g., display an error message to the user
    }
  };

  // Use useEffect to fetch weather data when location or unit changes
  useEffect(() => {
    if (location) {
      fetchWeatherData();
    }
  }, [location, unit]);

  return (
    <div>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          minHeight: "100vh",
          backgroundImage: `url(${BackgroundImg})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
        }}
      >
        {loading ? (
          <CircularProgress />
        ) : (
          <Container maxWidth="lg">
            <LocationInput setLocation={setLocation} />
            {forecastDataResponse && (
              <WeatherDetails
                weatherData={weatherData}
                unit={unit}
                setUnit={setUnit}
              />
            )}
            {forecastData && (
              <div>
                <h2>Weekly Forecast </h2>
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                  }}
                >
                  {forecastData &&
                    forecastData.map((data, index) => (
                      <>
                        <WeatherCard
                          date={new Date(data.date).toLocaleDateString(
                            "en-US",
                            {
                              weekday: "long",
                              day: "numeric",
                              month: "long",
                            }
                          )}
                          temperature={
                            data.temp + (unit === "metric" ? "°C" : "°F")
                          }
                          iconName="cloud"
                          iconColor="primary"
                          icon={data?.img_name}
                        />
                      </>
                    ))}
                </Box>
              </div>
            )}
          </Container>
        )}
      </Box>
    </div>
  );
};

export default WeatherWidget;

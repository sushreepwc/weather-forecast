import React from "react";
import { render } from "@testing-library/react";
import WeatherCard from "./Card";

test("renders WeatherCard component correctly", () => {
  const props = {
    day: "Monday",
    date: "2024-01-14",
    temperature: "25°C",
    icon: "01d",
  };

  const { getByText, getByAltText } = render(<WeatherCard {...props} />);

  // Assert that the rendered component contains the expected content
  expect(getByText(props.date)).toBeInTheDocument();
  expect(getByText(props.temperature)).toBeInTheDocument();
  expect(getByAltText("Weather Icon")).toBeInTheDocument();
});

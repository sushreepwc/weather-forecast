import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";

const WeatherCard = ({ day, date, temperature, icon }) => {
  return (
    <Card sx={{ maxWidth: 200, margin: 2 }}>
      <CardContent>
        <Typography variant="subtitle1">{date}</Typography>
        <Typography variant="body1">{temperature}</Typography>
        <img
          src={`https://openweathermap.org/img/w/${icon}.png`}
          alt="Weather Icon"
        />
      </CardContent>
    </Card>
  );
};

export default WeatherCard;

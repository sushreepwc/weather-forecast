# Weather Widget

Weather Widget is a web application that provides real-time weather information and a 7-day forecast for a specified location. It allows users to switch between metric and imperial units and provides a visually appealing interface for weather data.

## Features

- Current weather details including temperature, city name, and weather conditions.
- 7-day weather forecast with details for each day.
- Switch between Celsius and Fahrenheit units.
- Responsive design for a seamless user experience on various devices.

## Technologies Used

- React
- Axios for API requests
- Material-UI for UI components
- OpenWeatherMap API for weather data

## Installation

1. Clone the repository:

   git clone git@gitlab.com:sushreepwc/weather-forecast.git

### weather report screen

![weather report screen](./src/Screens/Screen_before_result.png)

### weather report screen after selected city

![weather report screen](./src/Screens/Screen_after_result.png)
